Basic Sort
============

A basic 🤩  sorting algorithm 


Usage 
-------------

+ To compile the program, use the command `make`
+ Run the program with the command  `make run` 
+ Extract datafiles to use as input with the command `make data`
+ Create the file **data.csv** with the command `make results`
    - This will run the program on all of the input data files and collate the results


